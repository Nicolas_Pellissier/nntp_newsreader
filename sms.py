import urllib.request
import urllib.parse
import configparser
import time

def readSMSCredential(filename):
    config = configparser.ConfigParser()
    config.read(filename)
    section = 'SMS_credential'
    return (config.items(section))

def splitMsg(msg):
    messages = []
    initialLen = len(msg)
    msgSize = 950
    count = 1
    while (len(msg) != 0):
        index = len(msg)
        if (len(msg) > msgSize):
            index = msg[:msgSize].rfind('.')
            if (index == -1):
                index = msg[:msgSize].rfind('+')
                if (index == -1):
                    index = msg[:msgSize].rfind('%') - 1
            index = index + 1 if index > -1 else msgSize
        messages.append(('..('+str(count)+')..' if len(msg) != initialLen else '') + msg[:index] + ('...' if index < len(msg) else ''))
        msg = msg[index:]
        count += 1
    return messages

def sendSMS(msg):
    msg = urllib.parse.quote_plus(msg)
    messages = splitMsg(msg)
    usersList = readSMSCredential('config.cfg')
    for user, pwd in usersList:
    #user, pwd = readSMSCredential('config.cfg')
        for m in messages:
            url='https://smsapi.free-mobile.fr/sendmsg?user='+user+'&pass='+pwd+'&msg='+m
            try:
                print('sending SMS')
                resp = urllib.request.urlopen(url)
                print('SMS sent')
                time.sleep(7)
            except IOError as e:
                print('error sending SMS: ', e)
                #return 1
    return 0
