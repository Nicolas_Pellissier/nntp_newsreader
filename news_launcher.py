#! /usr/bin/python3

import sys
import time
import datetime as dt
from subprocess import call

from sms import sendSMS

def main():
    while True:
        sendSMS('[INFO]\n['+
                dt.datetime.now().strftime('%d/%m/%Y - %H:%M')+
                ']\nNews Reader is Running')

        check = call(['./reader.py'])
        if check != 0:
            sendSMS('[INFO]\n['+
            dt.datetime.now().strftime("%d/%m/%Y - %H:%M")+
            ']\nNews Reader has stopped :(')
            time.sleep(5)


main()
