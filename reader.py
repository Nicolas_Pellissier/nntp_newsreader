#! /usr/bin/python3

import nntplib
import configparser
import time

from sms import sendSMS

def readConfig(filename):
    config = configparser.ConfigParser()
    config.read(filename)
    return config

def writeConfig(config, filename):
    with open(filename, 'w') as configFile:
        config.write(configFile)
        print ('config successfully updated.')

def getEncodingInHeader(header):
    encoding = ""
    for line in header[1][2]:
        l = str(line)
        if 'Content-Type' in l:
            params = l.split(';')
            for elt in params:
                if 'charset='.upper() in elt.upper():
                    encoding = elt[elt.index("=")+1:]
    return encoding

def reader():
    config = readConfig('config.cfg')
    nntpServerURL = config.get('server', 'name')

    # Open connection to the NNTP server
    s = nntplib.NNTP(nntpServerURL)

    for group in config['groups']:# for each group
        configHasChanged = False
        lastNewsRead = config['groups'][group]
        resp, count, first, last, name = s.group(group)
        if lastNewsRead == '-1':
            lastNewsRead = str(last)
            configHasChanged = True
        resp, overviews = s.over((lastNewsRead, last))
        for id, over in overviews:#foreach msg in a group
            #print (over)
            subject = '[subject error]'
            date = '[date error]'
            try:
                subject = nntplib.decode_header(over['subject'])
            except UnicodeDecodeError as e:
                pass
            try:
                date = nntplib.decode_header(over['date'])
            except UnicodeDecodeError as e:
                pass
            content = group + "\n------\n" + date + '\n------\n' + subject + "\n------\n"
            if str(id) > lastNewsRead:
                article_id = over['message-id']
                encoding = getEncodingInHeader(s.head(article_id))
                for line in s.body(article_id)[1][2]:
                    content += line.decode('utf-8' if encoding == "" else encoding, errors='replace') + "\n"
                content = content if content != "" else "No content"
                if sendSMS(content) == 0:
                    configHasChanged = True
        if configHasChanged:
            config['groups'][group] = str(last)
            writeConfig(config, 'config.cfg')

def main():
    error_occured = True
    while True:
        try:
            if error_occured:
                print('reading...')
                error_occured = False
            reader()
        except IOError as e:
            print('Error: ', e)
            error_occured = True
        time.sleep(20)

main()
